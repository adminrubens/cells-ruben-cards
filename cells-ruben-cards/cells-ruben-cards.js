{
  const {
    html,
  } = Polymer;
  /**
    `<cells-ruben-cards>` Description.

    Example:

    ```html
    <cells-ruben-cards></cells-ruben-cards>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-ruben-cards | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsRubenCards extends Polymer.Element {

    static get is() {
      return 'cells-ruben-cards';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="cells-ruben-cards-styles cells-ruben-cards-shared-styles"></style>
      <slot></slot>
      
          <p>Welcome to Cells</p>
      
      `;
    }
  }

  customElements.define(CellsRubenCards.is, CellsRubenCards);
}